package tile.square;

import java.awt.image.BufferedImage;

public class Square {

    private BufferedImage originalImage;
    private BufferedImage image;
    private Edge edge;
    private boolean collision;

    private int length;

    private double scale;

    private int scaledLength;

    private int squareX;
    private int squareY;
    //TODO implement squares know their location

    public Square() {
        setImage(null);
        setCollision(false);
        setEdge(Edge.NONE);
        setLength(0);
        setSquareX(0);
        setSquareY(0);
    }

    public Square(BufferedImage image, boolean collision, int length, double scale) {
        setOriginalImage(image);
        setImage(image);
        setCollision(collision);
        setEdge(Edge.NONE);
        setLength(length);
        setScale(scale);
        adjustScaleLength();
        setSquareX(0);
        setSquareY(0);
    }

    public Square(BufferedImage image, BufferedImage originalImage, boolean collision, int length, Edge edge, int squareX, int squareY, double scale,
                  int scaledLength) {

        setOriginalImage(originalImage);
        setImage(image);
        setCollision(collision);
        setEdge(edge);
        setLength(length);
        setScale(scale);
        setScaledLength(scaledLength);
        setSquareX(squareX);
        setSquareY(squareY);
    }
    public Square(Square square) {
        // COPY CONSTRUCTOR
        this(square.getImage(), square.getOriginalImage(),square.isCollision(), square.getLength(), square.getEdge(), square.getSquareX(),
                square.getSquareY(), square.getScale(), square.getScaledLength());
    }

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public int getScaledLength() {
        return scaledLength;
    }

    public void setScaledLength(int scaledLength) {
        this.scaledLength = scaledLength;
    }

    public void adjustScaleLength() {
        this.scaledLength = (int) (this.scale * this.length);
    }

    public int getSquareX() {
        return squareX;
    }

    public void setSquareX(int squareX) {
        this.squareX = squareX;
    }

    public int getSquareY() {
        return squareY;
    }

    public void setSquareY(int squareY) {
        this.squareY = squareY;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Edge getEdge() {
        return edge;
    }

    public void setEdge(Edge edge) {
        this.edge = edge;
    }

    public boolean isCollision() {
        return collision;
    }

    public void setCollision(boolean collision) {
        this.collision = collision;
    }
}
