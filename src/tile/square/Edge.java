package tile.square;

public enum Edge {
    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT
}
