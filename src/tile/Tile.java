package tile;

import engine.Updatable;
import engine.util.image.ImageScaler;
import tile.square.Edge;
import tile.square.Square;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

public class Tile implements Updatable {

    private Square[][] tilesSquares;

    private int length;

    private int scaledLength;

    private int lengthInSquares;
    private Square spawnSquare;

    //TODO remove tiles knowing their spawn square and have it handled by the gameworld??
    private int tileX;
    private int tileY;

    private double scale;

    private int stackOccurrence;
    // determines how many of each tile preset should be in the stack


    public Tile() {
        setLength(0);
        setLengthInSquares(0);
        setTileX(0);
        setTileY(0);
        setTilesSquares(new Square[getLength()][getLength()]);
        setSpawnSquare(null);
        setStackOccurrence(0);
        setEdgeType();
    }

    public Tile(int length, double scale, int lengthInSquares, int stackOccurrence, Square[][] squares) {
        setLength(length);
        setLengthInSquares(lengthInSquares);
        setScale(scale);
        adjustScaledLength();
        setTileX(0);
        setTileY(0);
        setTilesSquares(new Square[getLength()][getLength()]);
        fillTilesSquares(squares);
        setSpawnSquare(null);
        setStackOccurrence(stackOccurrence);
        setEdgeType();
    }
    public Tile(int length, int lengthInSquares, int stackOccurrence, Square[][] squares, int tileX, int tileY,
                Square spawnSquare, double scale, int scaledLength) {
        setLength(length);
        setScale(scale);
        setScaledLength(scaledLength);
        setLengthInSquares(lengthInSquares);
        setTileX(tileX);
        setTileY(tileY);
        setTilesSquares(squares);
        setSpawnSquare(spawnSquare);
        setStackOccurrence(stackOccurrence);
        setEdgeType();
    }

    public Tile(Tile tile) {
        // COPY CONSTRUCTOR
        this(tile.getLength(), tile.getLengthInSquares(), tile.getStackOccurrence(), tile.getTilesSquares(),
                tile.getTileX(), tile.getTileY(), tile.getSpawnSquare(), tile.getScale(), tile.getScaledLength());
    }

    public void update(double scale) {
        for (int i = 0; i < getLengthInSquares(); i++) {
            for (int j = 0; j < getLengthInSquares(); j++) {
                Square square = getTilesSquares()[i][j];
                square.setScale(scale);
                square.adjustScaleLength();
                BufferedImage image = ImageScaler.scaleImage(square.getOriginalImage(),
                        square.getScaledLength(), square.getScaledLength());

                getTilesSquares()[i][j].setImage(image);
            }
        }
    }



    public void setTileSpawn(Random rand) {
        ArrayList<Square> validSquares= new ArrayList<>();
        for (int row = 0; row < getLengthInSquares(); row++) {
            for (int col = 0; col < getLengthInSquares(); col++) {
                Square square = getTilesSquares()[row][col];
                if (!square.isCollision() && square.getEdge() == Edge.NONE) {
                    validSquares.add(square);
                }
            }
        }
        int randNum = rand.nextInt(validSquares.size());
        setSpawnSquare(validSquares.get(randNum));
    }
    private void setEdgeType() {
        // TODO This is broken for if a no collision tile is on two edge types
        int firstRow = 0;
        int lastRow = getLengthInSquares()-1;
        int firstCol = 0;
        int lastCol = getLengthInSquares()-1;

        for (int row = 0; row < getLengthInSquares(); row++) {
            for (int col = 0; col < getLengthInSquares(); col++) {
                boolean collision = getTilesSquares()[row][col].isCollision();

                if (row == firstRow && !collision) {
                    getTilesSquares()[row][col].setEdge(Edge.UP);
                } else if (row == lastRow && !collision) {
                    getTilesSquares()[row][col].setEdge(Edge.DOWN);
                } else if (col == firstCol && !collision) {
                    getTilesSquares()[row][col].setEdge(Edge.LEFT);
                } else if (col == lastCol && !collision) {
                    getTilesSquares()[row][col].setEdge(Edge.RIGHT);
                }
            }
        }
    }

    public int getScaledLength() {
        return scaledLength;
    }

    public void setScaledLength(int scaledLength) {
        this.scaledLength = scaledLength;
    }

    public void adjustScaledLength() {
        this.scaledLength = (int) (this.scale * this.length);
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public int getTileX() {
        return tileX;
    }

    public void setTileX(int tileX) {
        this.tileX = tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void setTileY(int tileY) {
        this.tileY = tileY;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLengthInSquares() {
        return lengthInSquares;
    }

    public void setLengthInSquares(int lengthInSquares) {
        this.lengthInSquares = lengthInSquares;
    }

    public Square getSpawnSquare() {
        return spawnSquare;
    }

    public int getStackOccurrence() {
        return stackOccurrence;
    }

    public void setStackOccurrence(int stackOccurrence) {
        this.stackOccurrence = stackOccurrence;
    }

    public void setSpawnSquare(Square spawnSquare) {
        this.spawnSquare = spawnSquare;
    }

    public Square[][] getTilesSquares() {
        return tilesSquares;
    }

    public void setTilesSquares(Square[][] tilesSquares) {
        this.tilesSquares = tilesSquares;
    }

    public void fillTilesSquares(Square[][] squares) {
        for (int row = 0; row < getLengthInSquares(); row++) {
            for (int col = 0; col < getLengthInSquares(); col++) {
                getTilesSquares()[row][col] = squares[row][col];
            }
        }
    }


}
