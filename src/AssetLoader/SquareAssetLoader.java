package AssetLoader;

import engine.util.AssetLoader;
import engine.util.FileLineCounter;
import engine.util.image.ImageLoader;
import tile.square.Square;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SquareAssetLoader extends AssetLoader {

    public static Square[] loadSquareAssets(int length, double scale) {
        AssetLoader.setAssetFilePath("/tiles/squares/square_assets.txt");
        AssetLoader.setMaxNumOfAssets(FileLineCounter.count(AssetLoader.getAssetFilePath()) - 1);

        Square[] squareAssets = new Square[AssetLoader.getMaxNumOfAssets()];

        try {
            InputStream is = SquareAssetLoader.class.getResourceAsStream(AssetLoader.getAssetFilePath());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Skip the first line
            br.readLine();
            for (int i = 0; i < AssetLoader.getMaxNumOfAssets(); i++) {
                String line = br.readLine();
                if (!line.matches("")) {
                    String[] splitLines = line.split(" ");
                    String filename = splitLines[0];
                    boolean collision = Boolean.parseBoolean((splitLines[1]));

                    squareAssets[i] = new Square(ImageLoader.loadImage("/tiles/squares/", filename, length), collision, length, scale);
                }
            }
            br.close();
            is.close();
            return squareAssets;

        } catch (IOException e) {
            System.out.println("Unable to load square");
            throw new RuntimeException(e);
        }
    }

}
