package AssetLoader;

import engine.util.AssetLoader;
import engine.util.FileLineCounter;
import tile.square.Square;
import tile.Tile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TileAssetLoader extends AssetLoader {

    private static Square[][] loadTileAsset(int tileLengthInSquares, String tileAsset, Square[] squareAssets) {
        Square[][] squares = new Square[tileLengthInSquares][tileLengthInSquares];

        String filename = "/tiles/" + tileAsset + ".txt";
        try {
            InputStream is = TileAssetLoader.class.getResourceAsStream(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            for (int row = 0; row < tileLengthInSquares; row++) {
                String line = br.readLine();
                String[] splitLines = line.split(" ");

                for (int col = 0; col < tileLengthInSquares; col++) {
                        int typeIndex = Integer.parseInt(splitLines[col]);
                        Square square = new Square(squareAssets[typeIndex]);
                        square.setSquareX(col);
                        square.setSquareY(row);
                        squares[row][col] = square;
                    }
                }
            br.close();
            is.close();

        } catch (IOException e) {
            System.out.println("Unable to load tile");
            throw new RuntimeException(e);
        }
        return squares;
    }

    public static Tile[] loadTileAssets(int squareLength, int tileLength, double scale) {
        Square[] squareAssets = SquareAssetLoader.loadSquareAssets(squareLength, scale);

        int tileLengthInSquares = tileLength / squareLength;
        setAssetFilePath("/tiles/tile_assets.txt");
        setMaxNumOfAssets(FileLineCounter.count(getAssetFilePath()) - 1);


        Tile[] tileAssets = new Tile[getMaxNumOfAssets()];

        try {
            InputStream is = TileAssetLoader.class.getResourceAsStream(getAssetFilePath());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Skip the first line
            br.readLine();
            for (int i = 0; i < getMaxNumOfAssets(); i++) {
                String line = br.readLine();
                if (!line.matches("")) {
                    String[] splitLines = line.split(" ");
                    String tileType = splitLines[0];
                    int stackOccurrence = Integer.parseInt(splitLines[1]);
                    Square[][] squares = loadTileAsset(tileLengthInSquares, tileType, squareAssets);
                    tileAssets[i] = new Tile(tileLength, scale, tileLengthInSquares,stackOccurrence, squares);
                }
            }

            br.close();
            is.close();
            return tileAssets;


        } catch (IOException e) {
            System.out.println("Unable to load tiles types");
            throw new RuntimeException(e);
        }

    }

}
