package engine.sound;

public class SoundManager implements MusicPlayer {

    private Sound music;

    private Sound effect;

    public SoundManager() {
        setMusic(new Sound());
        setEffect(new Sound());
    }

    public void playMusic(int i) {
        getMusic().setFile(i);
        getMusic().play();
        getMusic().loop();
    }

    public void stopMusic() {
        getMusic().stop();
    }

    public void playEffect(int i) {
        getEffect().setFile(i);
        getEffect().play();
    }

    public Sound getMusic() {
        return music;
    }

    public void setMusic(Sound music) {
        this.music = music;
    }

    public Sound getEffect() {
        return effect;
    }

    public void setEffect(Sound soundEffect) {
        this.effect = soundEffect;

    }
}
