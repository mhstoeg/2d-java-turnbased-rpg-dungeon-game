package engine.actions;

import engine.input.InputDevice;

public class ActionLoader {

    private Action[] actions;

    public ActionLoader() {
        setActions(new Action[ActionType.values().length]);
        setupActions();
    }

    public void setupActions() {
/*
        int i = 0;
        for (ActionType type : ActionType.values()) {
            getActions()[i] = new Action(input, type);
            i++;
        }
    public void determineActions() {
        for (Action action : actionManager.getActions()) {
            if (getInputHandler().isActivated(action.getInput(), action.getInputType())) {
                getActionQueue().addAction(action.getActionType());
            }
        }
    }
*/
        //TODO this class could be changed into a loader class for reading the keybindings into the game.


        //TODO create a Loader, and config reader and writer. for use in configs and assets
        // the input type should automatically be determed by the input given
        // Need someway to prevent mapping the same input to multiple inputs
        // maybe chnage input string to a ENUM?
        getActions()[0] = new Action("W", InputDevice.KEYBOARD, ActionType.MOVE_CAMERA_UP);
        getActions()[1] = new Action("S", InputDevice.KEYBOARD, ActionType.MOVE_CAMERA_DOWN);
        getActions()[2] = new Action("A", InputDevice.KEYBOARD, ActionType.MOVE_CAMERA_LEFT);
        getActions()[3] = new Action("D", InputDevice.KEYBOARD, ActionType.MOVE_CAMERA_RIGHT);
        getActions()[4] = new Action("SCROLL_PRESSED", InputDevice.MOUSE, ActionType.PAN_CAMERA);
        getActions()[5] = new Action("MW_DOWN", InputDevice.MOUSE, ActionType.ZOOM_OUT);
        getActions()[6] = new Action("MW_UP", InputDevice.MOUSE, ActionType.ZOOM_IN);
    }

    public Action[] getActions() {
        return actions;
    }

    public void setActions(Action[] actions) {
        this.actions = actions;
    }

}
