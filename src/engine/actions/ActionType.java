package engine.actions;

public enum ActionType {
    PAN_CAMERA,
    MOVE_CAMERA_LEFT,
    MOVE_CAMERA_RIGHT,
    MOVE_CAMERA_UP,
    MOVE_CAMERA_DOWN,
    ZOOM_OUT,
    ZOOM_IN,
}
