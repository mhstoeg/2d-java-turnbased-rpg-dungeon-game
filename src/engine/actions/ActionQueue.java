package engine.actions;

import engine.util.data_structure.Queue;

public class ActionQueue {

    private Queue queue;

    public ActionQueue() {
        setQueue(new Queue());
    }

    public void addAction(ActionType actionType) {
        getQueue().enqueue(actionType);
    }

    public boolean unprocessedActions() {
        return !getQueue().isEmpty();
    }

    public ActionType processAction() {
        if (unprocessedActions()) {
            return (ActionType) getQueue().dequeue().getKey();
        }
        else {
            return null;
        }
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }
}
