package engine.actions;

import engine.input.InputDevice;

public class Action {

    //TODO Should have a object for input that uses input and inputType??
    private String input;

    private InputDevice inputType;
    private ActionType actionType;

    public Action(String input, InputDevice inputType, ActionType actionType) {
        setInput(input);
        setActionType(actionType);
        setInputType(inputType);
    }


    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public InputDevice getInputType() {
        return inputType;
    }

    public void setInputType(InputDevice inputType) {
        this.inputType = inputType;
    }
}
