package engine;

import tile.square.Square;
import tile.Tile;

public class Camera {

    private int worldX;
    private int worldY;
    private int screenWidth;
    private int screenHeight;

    private int speed;


    public Camera(int worldX, int worldY, int screenWidth, int screenHeight, int speed) {
        setWorldX(worldX);
        setWorldY(worldY);
        setScreenHeight(screenHeight);
        setScreenWidth(screenWidth);
        setSpeed(speed);
    }

    public boolean isTileWithin(Tile tile) {
        boolean within = false;

        int tileLength = tile.getScaledLength();
        int tileWorldX = (tile.getTileX() * tileLength) + getWorldX();
        int tileWorldY = (tile.getTileY() * tileLength) + getWorldY();

        if(tileWorldX > -tileLength && tileWorldX <  getScreenWidth()
                && tileWorldY > -tileLength && tileWorldY < getScreenHeight()) {
            within = true;
        }

        return within;
    }

    // TODO Maybe move these methods in their class and implement interface that includes update, render
    // isWithinCamera, attribute like screenX and screenY and so on
    public boolean isSquareWithin(Square square, int tileScreenX, int tileScreenY) {
        boolean within = false;

        int squareLength = square.getScaledLength();
        int squareWorldX = ((square.getSquareX() * squareLength) + tileScreenX + getWorldX());
        int squareWorldY = ((square.getSquareY() * squareLength) + tileScreenY + getWorldY());

        if(squareWorldX > -squareLength && squareWorldX  < getScreenWidth() &&
                squareWorldY  > -squareLength && squareWorldY  < getScreenHeight()){
            within = true;
        }

        return within;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void moveCamera(int x, int y) {
        setWorldX(getWorldX() + x);
        setWorldY(getWorldY() + y);
    }


    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public int getWorldX() {
        return worldX;
    }

    public void setWorldX(int worldX) {
        this.worldX = worldX;
    }

    public int getWorldY() {
        return worldY;
    }

    public void setWorldY(int worldY) {
        this.worldY = worldY;
    }

}
