package engine;

import AssetLoader.TileAssetLoader;
import tile.Tile;

import java.util.Collections;
import java.util.Stack;

public abstract class AssetSetter {

    public static void setupTileStack(int squareLength, int tileLengthInSquares, double scale, Stack<Tile> tileStack) {
        Tile[] tilePresets = TileAssetLoader.loadTileAssets(squareLength, squareLength * tileLengthInSquares, scale);
        for (Tile tilePreset : tilePresets) {
            for (int j = 0; j < tilePreset.getStackOccurrence(); j++) {
                tileStack.add(new Tile(tilePreset));
            }
        }
        // Shuffle the stack
        Collections.shuffle(tileStack);
    }
}
