package engine.input;

public enum InputDevice {
    MOUSE,
    KEYBOARD,
    GAMEPAD
}
