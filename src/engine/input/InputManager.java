package engine.input;

import engine.Updatable;
import engine.input.keyboard.KeyAdapter;
import engine.input.mouse.MouseAdapter;

public class InputManager implements Updatable {

    private KeyAdapter keyAdapter;
    private MouseAdapter mouseAdapter;
    private int dragAmountX;

    private int dragAmountY;

    //TODO find a better name for mouseX and Y as these are just use int he calculation of dragAmount
    private int mouseX;
    private int mouseY;
    private int draggedCounter;

    public InputManager() {
        setKeyAdapter(new KeyAdapter());
        // TODO currently we store key values in the queue, this will need to be changed to accomodate diffrent
        // types of input that use the same values
        // could make and input class with type, and value
        setMouseAdapter(new MouseAdapter());
        resetDraggedCounter();
        setMouseX(0);
        setMouseY(0);
        setDragAmountX(0);
        setDragAmountY(0);
    }

    public void update() {
        updateDragValues();
    }

    public void updateDragValues() {
        if (getMouseAdapter().isDragged()) {
            if (draggedCounter == 0) {
                resetDragValues();

                setMouseX(getMouseAdapter().getX());
                setMouseY(getMouseAdapter().getY());

            }
            if (getDraggedCounter() == 1) {
                int x2 = getMouseX();
                int y2 = getMouseY();

                setMouseX(getMouseAdapter().getX());
                setMouseY(getMouseAdapter().getY());

                int x1 = getMouseX();
                int y1 = getMouseY();

                setDragAmountX((x2 - x1));
                setDragAmountY((y2 - y1));

                resetDraggedCounter();
            }
            incrementDraggedCounter();
        }

        else {
            resetDraggedCounter();
            resetDragValues();
        }
    }

    public boolean isActivated(String input, InputDevice type) {
        boolean isActivated = false;
        if (type == InputDevice.KEYBOARD) {
            isActivated = getKeyAdapter().isKeyPressed(input);
        }
        else if (type == InputDevice.MOUSE) {
            isActivated = getMouseAdapter().isActivated(input);
        }
        return isActivated;
    }

    public void resetDragValues() {
        resetDragX();
        resetDragY();
    }
    public int getDragAmountX() {
        return dragAmountX;
    }

    public void setDragAmountX(int dragAmountX) {
        this.dragAmountX = dragAmountX;
    }

    public int getDragAmountY() {
        return dragAmountY;
    }

    public void setDragAmountY(int dragAmountY) {
        this.dragAmountY = dragAmountY;
    }

    public void resetDragX() {
        this.dragAmountX = 0;
    }

    public void resetDragY() {
        this.dragAmountY = 0;
    }
    private int getMouseX() {
        return mouseX;
    }

    private void setMouseX(int mouseX) {
        if (mouseX >= 0) {
            this.mouseX = mouseX;
        }
    }

    private int getMouseY() {
        return mouseY;
    }

    private void setMouseY(int mouseY) {
        if (mouseY >= 0) {
            this.mouseY = mouseY;
        }
    }

    private void resetDraggedCounter() {
        draggedCounter = 0;
    }

    private void incrementDraggedCounter() {
        draggedCounter = draggedCounter + 1;
    }

    private int getDraggedCounter() {
        return draggedCounter;
    }
    public KeyAdapter getKeyAdapter() {
        return keyAdapter;
    }

    private void setKeyAdapter(KeyAdapter keyAdapter) {
        this.keyAdapter = keyAdapter;
    }

    public MouseAdapter getMouseAdapter() {
        return mouseAdapter;
    }

    private void setMouseAdapter(MouseAdapter mouseAdapter) {
        this.mouseAdapter = mouseAdapter;
    }
}
