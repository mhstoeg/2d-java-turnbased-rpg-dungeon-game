package engine.input.keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyAdapter implements KeyListener {
    private boolean enterPressed, spacePressed;
    private boolean wPressed, sPressed, aPressed, dPressed;

    public KeyAdapter() {
        setEnterPressed(false);
        setSpacePressed(false);
        setWPressed(false);
        setAPressed(false);
    }
    public boolean isEnterPressed() {
        return enterPressed;
    }


    public boolean isSpacePressed() {
        return spacePressed;
    }

    private void setEnterPressed(boolean enterPressed) {
        this.enterPressed = enterPressed;
    }

    private void setSpacePressed(boolean spacePressed) {
        this.spacePressed = spacePressed;
    }

    public boolean isWPressed() {
        return wPressed;
    }

    public void setWPressed(boolean wPressed) {
        this.wPressed = wPressed;
    }

    public boolean isSPressed() {
        return sPressed;
    }

    public void setSPressed(boolean sPressed) {
        this.sPressed = sPressed;
    }

    public boolean isAPressed() {
        return aPressed;
    }

    public void setAPressed(boolean aPressed) {
        this.aPressed = aPressed;
    }

    public boolean isDPressed() {
        return dPressed;
    }

    public void setDPressed(boolean dPressed) {
        this.dPressed = dPressed;
    }

    public boolean isKeyPressed(String key) {
        boolean isPressed = false;
        switch (key) {
            case "W":
                isPressed = isWPressed();
                break;
            case "S":
                isPressed = isSPressed();
                break;
            case "A":
                isPressed = isAPressed();
                break;
            case "D":
                isPressed = isDPressed();
                break;
            case "ENTER":
                isPressed = isEnterPressed();
                break;
            case "SPACE":
                isPressed = isSpacePressed();
                break;
        }
        return isPressed;
    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }


    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();

        if (code == KeyEvent.VK_ENTER) {
            setEnterPressed(true);
        }

        if (code == KeyEvent.VK_SPACE) {
            setSpacePressed(true);
        }

        if (code == KeyEvent.VK_W) {
            setWPressed(true);
        }

        if (code == KeyEvent.VK_S) {
            setSPressed(true);
        }

        if (code == KeyEvent.VK_A) {
            setAPressed(true);
        }

        if (code == KeyEvent.VK_D) {
            setDPressed(true);
        }
    }


    @Override
    public void keyReleased(KeyEvent keyEvent) {

        int code = keyEvent.getKeyCode();

        if (code == KeyEvent.VK_ENTER) {
            setEnterPressed(false);
        }

        if (code == KeyEvent.VK_SPACE) {
            setSpacePressed(false);
        }

        if (code == KeyEvent.VK_W) {
            setWPressed(false);
        }

        if (code == KeyEvent.VK_S) {
            setSPressed(false);
        }

        if (code == KeyEvent.VK_A) {
            setAPressed(false);
        }

        if (code == KeyEvent.VK_D) {
            setDPressed(false);
        }
    }

}
