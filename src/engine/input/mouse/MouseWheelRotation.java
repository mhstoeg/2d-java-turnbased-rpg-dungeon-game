package engine.input.mouse;

public enum MouseWheelRotation {

    DOWN(1),
    UP(-1);

    private int value;

    MouseWheelRotation (int value) {
        setValue(value);
    }
    public int getValue() {
        return value;
    }

    private void setValue(int value) {
        this.value = value;
    }
}
