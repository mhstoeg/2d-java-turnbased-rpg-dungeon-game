package engine.input.mouse;

public class MouseAdapter {


    private MouseButtonAdapter mouseButtonAdapter;
    private MouseWheelAdapter mouseWheelAdapter;

    private MouseMotionAdapter mouseMotionAdapter;

    public MouseAdapter() {
        setMouseButtonAdapter(new MouseButtonAdapter());
        setMouseWheelAdapter(new MouseWheelAdapter());
        setMouseMotionAdapter(new MouseMotionAdapter());
    }


    public boolean isMwUp() { return getMouseWheelAdapter().isMwUp();}

    public boolean isMwDown() { return getMouseWheelAdapter().isMwDown();}

    public boolean isDragged() { return getMouseMotionAdapter().isDragged();}
    public boolean isMoved() { return getMouseMotionAdapter().isMoved();}

    public boolean isScrollPressed() { return getMouseButtonAdapter().isScrollPressed();}

    public int getX() {return getMouseMotionAdapter().getX();}

    public int getY() {return getMouseMotionAdapter().getY();}

    public MouseButtonAdapter getMouseButtonAdapter() {
        return mouseButtonAdapter;
    }

    public void setMouseButtonAdapter(MouseButtonAdapter mouseButtonAdapter) {
        this.mouseButtonAdapter = mouseButtonAdapter;
    }

    public MouseWheelAdapter getMouseWheelAdapter() {
        return mouseWheelAdapter;
    }

    public void setMouseWheelAdapter(MouseWheelAdapter mouseWheelAdapter) {
        this.mouseWheelAdapter = mouseWheelAdapter;
    }

    public MouseMotionAdapter getMouseMotionAdapter() {
        return mouseMotionAdapter;
    }

    public void setMouseMotionAdapter(MouseMotionAdapter mouseMotionAdapter) {
        this.mouseMotionAdapter = mouseMotionAdapter;
    }

    public boolean isActivated(String input) {
        boolean isActivated = false;
        switch (input) {
            //TODO Add other mouse buttons
            case "MW_UP":
                isActivated = isMwUp();
                break;
            case "MW_DOWN":
                isActivated = isMwDown();
                break;
            case "SCROLL_PRESSED":
                isActivated = isScrollPressed();
        }
        return isActivated;
    }

}
