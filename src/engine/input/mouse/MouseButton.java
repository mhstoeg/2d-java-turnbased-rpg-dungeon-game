package engine.input.mouse;

public enum MouseButton {
    LMB(1),
    SCROLL(2),
    RMB(3);

    private int value;

    MouseButton(int value) {
        setValue(value);
    }
    public int getValue() {
        return value;
    }

    private void setValue(int value) {
        this.value = value;
    }


}
