package engine.input.mouse;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class MouseWheelAdapter implements MouseWheelListener {

    private boolean mwUp, mwDown;

    public MouseWheelAdapter() {
        setMwDown(false);
        setMwUp(false);
    }

    public boolean isMwUp() {
        boolean previousState = mwUp;
        setMwUp(false);
        return previousState;
    }

    private void setMwUp(boolean mwUp) {
        this.mwUp = mwUp;
    }

    public boolean isMwDown() {
        boolean previousState = mwDown;
        setMwDown(false);
        return previousState;
    }


    private void setMwDown(boolean mwDown) {
        this.mwDown = mwDown;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        int rotation = mouseWheelEvent.getWheelRotation();

        if (rotation == MouseWheelRotation.UP.getValue()) {
            setMwUp(true);
        }

        if (rotation == MouseWheelRotation.DOWN.getValue()) {
            setMwDown(true);
        }
    }

}
