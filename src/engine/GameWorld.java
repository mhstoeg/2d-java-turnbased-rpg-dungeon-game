package engine;

import tile.Tile;

import java.util.*;

public class GameWorld implements Updatable {

    private int width;
    private int height;

    private int maxRow;
    private int maxCol;

    //TODO MOVE TILE RELATED CODE INTO ANOTHER CLASS CALLED TILE MANAGER
    private ArrayList<Tile> revealedTiles;
    private Stack<Tile> tileStack;

    public GameWorld(int squareLength, double scale, int tileLengthInSquares, int maxTilemapRow, int maxTilemapCol,
                     Random rand) {
        setMaxRow(maxTilemapRow);
        setMaxCol(maxTilemapCol);

        setWidth(squareLength * tileLengthInSquares * getMaxRow());
        setHeight(squareLength * tileLengthInSquares * getMaxCol());

        setTileStack(new Stack<>());
        setRevealedTiles(new ArrayList<>());

        AssetSetter.setupTileStack(squareLength, tileLengthInSquares, scale, getTileStack());

        setupGame();


        revealTile(0,0, rand);
        revealTile(1,0, rand);
        revealTile(2,0, rand);
        revealTile(0,1, rand);
        revealTile(1,1, rand);
        revealTile(2,1, rand);
    }

    public void setupGame() {
/*        // Initialise all game objects
        aSetter.setObject();
        aSetter.setNPC();
        aSetter.setMonster();
        gameState = ;
        debugModeOn = false; */
        //setFullScreen();

    }

    public void revealTile(int tileX, int tileY, Random rand) {
        Tile tile = getTileStack().pop();
        tile.setTileX(tileX);
        tile.setTileY(tileY);
        tile.setTileSpawn(rand);
        getRevealedTiles().add(tile);
        setMaxCol(tileX);
        setMaxRow(tileY);

    }

    public void update(double scale, boolean scaledChanged) {

        // Tiles
        if (scaledChanged) {
            for (Tile tile : getRevealedTiles()) {
                tile.setScale(scale);
                tile.adjustScaledLength();
                tile.update(scale);
            }
        }
    }

    public Stack<Tile> getTileStack() {
        return tileStack;
    }

    public void setTileStack(Stack<Tile> tileStack) {
        this.tileStack = tileStack;
    }

    public ArrayList<Tile> getRevealedTiles() {
        return revealedTiles;
    }

    public void setRevealedTiles(ArrayList<Tile> revealedTiles) {
        this.revealedTiles = revealedTiles;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxRow() {
        return maxRow;
    }

    public void setMaxRow(int maxRow) {
        if (maxRow >= 0 && maxRow > getMaxRow()) {
            this.maxRow = maxRow;
        }
    }

    public int getMaxCol() {
        return maxCol;
    }

    public void setMaxCol(int maxCol) {
        if (maxCol >= 0 && maxCol > getMaxRow()) {
            this.maxCol = maxCol;
        }
    }
}
