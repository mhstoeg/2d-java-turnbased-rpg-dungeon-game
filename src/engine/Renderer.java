package engine;

import tile.square.Square;
import tile.Tile;

import java.awt.*;

public class Renderer implements Updatable {

    // FPS
    private int fps;
    private int fpsCap;
    private boolean fpsCapOn;
    private int frameCount;

    // RENDER SCALE
    private double scale;
    private boolean scaleChanged;

    public Renderer(int fpsCap, boolean fpsCapOn) {

        // FPS
        setFps(0);
        setFpsCap(fpsCap);
        setFpsCapOn(fpsCapOn);
        setFrameCount(0);

        // RENDER SCALE
        setScale(1); // TODO HARDCODED
        setScaleChanged(false);

    }


    public void update() {
        setScaleChanged(false);
    }


    public void render(GameWorld gameWorld, Camera camera, int tickRate) {

        // GAME WORLD
        renderGameWorld(gameWorld, camera);

        //TODO ONSCREEN STATS
/*
        g2.setColor(Color.white);
        g2.drawString("FPS " + getFps(), 10, 700);
        g2.drawString("TICK " + tickRate, 10, 800);
        g2.drawString("Camera X " + camera.getWorldX(), 10, 500);
        g2.drawString("Camera Y " + camera.getWorldY(), 10, 600);
*/


        //g2.dispose();
        incrementFrameCount();
    }

    public void renderGameWorld(GameWorld gameWorld, Camera camera) {
        renderTiles(gameWorld, camera);
    }

    public void renderTiles(GameWorld gameWorld, Camera camera) {
        for (Tile tile : gameWorld.getRevealedTiles()) {
            if (camera.isTileWithin(tile)) {
                renderTile(tile, camera);
                renderSpawnSquare(tile, camera);
            }
        }
    }
    public void renderTile(Tile tile, Camera camera){
        int tileLength = tile.getScaledLength();
        int tileScreenX = tile.getTileX() * tileLength;
        int tileScreenY = tile.getTileY() * tileLength;
        int squareLength = tileLength / tile.getLengthInSquares();
        int squareStartX = tileScreenX + camera.getWorldX();
        int squareStartY = tileScreenY + camera.getWorldY();
        int squareX = squareStartX;
        int squareY = squareStartY;
        for (int row = 0; row < tile.getLengthInSquares(); row++) {
            for (int col = 0; col < tile.getLengthInSquares(); col++) {
                Square square = tile.getTilesSquares()[row][col];

                if (camera.isSquareWithin(square, tileScreenX, tileScreenY)) {

                    //TODO Constantly rescaling an image when scaling does not occur
                    // heavily tanks the frame rate. Need to only scale when a change occurs
                    // Should probably update game length when a scale occurs.
                    //g2.drawImage(square.getImage(), squareX, squareY, null);
                }
                squareX += squareLength;
            }
            squareX = squareStartX;
            squareY += squareLength;
        }
    }

    public void renderSpawnSquare(Tile tile, Camera camera) {
        // TODO could pass a fuction of what to render since the above method is the same
        int tileLength = tile.getScaledLength();
        int tileScreenX = tile.getTileX() * tileLength;
        int tileScreenY = tile.getTileY() * tileLength;
        int squareLength = tileLength / tile.getLengthInSquares();
        int squareStartX = tileScreenX + camera.getWorldX();
        int squareStartY = tileScreenY + camera.getWorldY();
        int squareX = squareStartX;
        int squareY = squareStartY;
        for (int row = 0; row < tile.getLengthInSquares(); row++) {
            for (int col = 0; col < tile.getLengthInSquares(); col++) {
                Square square = tile.getTilesSquares()[row][col];

                if (camera.isSquareWithin(square, tileScreenX, tileScreenY)) {

                    if (square == tile.getSpawnSquare()) {

                        //g2.setColor(Color.red);
                        //g2.drawRect(squareX, squareY, squareLength, squareLength);
                    }
                }
                squareX += squareLength;
            }
            squareX = squareStartX;
            squareY += squareLength;
        }
    }
    public int getFps() {
        return fps;
    }

    public void setFps(int fps) {
        if (fps >= 0) {
            this.fps = fps;
        }
    }

    public int getFpsCap() {
        return fpsCap;
    }

    public void setFpsCap(int fpsCap) {
        if (fpsCap > 0) {
            this.fpsCap = fpsCap;
        }
    }
    public boolean isFpsCapOn() {
        return fpsCapOn;
    }

    public void setFpsCapOn(boolean fpsCapOn) {
        this.fpsCapOn = fpsCapOn;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public void setFrameCount(int frameCount) {
        if (frameCount >= 0) {
            this.frameCount = frameCount;
        }
    }

    public void incrementFrameCount() {
        this.frameCount++;
    }

    public boolean isScaleChanged() {
        return scaleChanged;
    }

    public void setScaleChanged(boolean scaleChanged) {
        this.scaleChanged = scaleChanged;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }
}
