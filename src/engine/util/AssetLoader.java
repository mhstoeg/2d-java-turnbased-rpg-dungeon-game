package engine.util;

public abstract class AssetLoader {


    static String assetFilePath;
    static int maxNumOfAssets;

    public static String getAssetFilePath() {
        return assetFilePath;
    }

    public static void setAssetFilePath(String assetFilePath) {
        AssetLoader.assetFilePath = assetFilePath;
    }

    public static int getMaxNumOfAssets() {
        return maxNumOfAssets;
    }

    public static void setMaxNumOfAssets(int maxNumOfAssets) {
        AssetLoader.maxNumOfAssets = maxNumOfAssets;
    }
}
