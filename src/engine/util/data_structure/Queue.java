package engine.util.data_structure;


public class Queue {

    private Node front, rear;

    public Queue() {
        setRear(null);
        setFront(getRear());
    }

    public void enqueue(Object o) {

        // Create a new node
        Node node = new Node(o);

        // If Queue is empty, the node is both front and rear
        if (isEmpty()) {
            setRear(node);
            setFront(getRear());
        }

        else {
            // Add the node to the end of the queue and set rear
            getRear().setNext(node);
            setRear(node);
        }

    }

    public Node peek() {
        Node node;
        if (!isEmpty()) {
            node = getFront();
        }
        else {
            node = null;
        }
        return node;
    }


    public Node dequeue() {
        Node node = peek();
        if (!isEmpty()) {
            setFront(getFront().getNext());

            // If the queue becomes empty, the rear node was just dequeued, and so rear becomes null
            if (getFront() == null) {
                setRear(null);
            }
        }
        return node;
    }

    public Node getFront() {
        return front;
    }

    public void setFront(Node front) {
        this.front = front;
    }

    public Node getRear() {
        return rear;
    }

    public void setRear(Node rear) {
        this.rear = rear;
    }

    public boolean isEmpty() {
        return front == null && rear == null;
    }
}
