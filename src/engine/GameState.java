package engine;

public enum GameState {
    PLAY,
    PAUSE,
    DIALOGUE,
    MENU,
    TITLE

}
