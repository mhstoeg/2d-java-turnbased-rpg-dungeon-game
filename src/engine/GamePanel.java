package engine;

import engine.actions.ActionManager;
import engine.sound.SoundManager;
import engine.util.OSValidator;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.stb.*;


import java.awt.*;
import java.nio.IntBuffer;
import java.util.Random;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class GamePanel implements Runnable, Updatable {

    // WINDOW SETTINGS
    private int screenWidth;
    private int screenHeight;

    // WINDOW
    private long window;

    // SYSTEM
    private Renderer renderer;
    private ActionManager actionManager;
    private SoundManager soundManager;
    private Random rand;
    private Camera camera;


    // GAME TICK RATE
    private final int tickRateCap;
    private int tickRate;

    // ENTITY AND OBJECT Maybe moved within a function??
    private GameWorld gameWorld;

    // GAME STATE
    private GameState gameState;

    public GamePanel(int tickRateCap, int fpsCap, boolean fpsCapOn) {

        //TODO Seperate all game specific code out of the engine, but used by the engine

        // SCREEN
        setScreenHeight(1080); //TODO HARDCODED
        setScreenWidth(1920); //TODO HARDCODED

        // SYSTEM
        setRenderer(new Renderer(fpsCap, fpsCapOn));
        setActionManager(new ActionManager());
        setSoundManager(new SoundManager());
        setRand(new Random());
        setCamera(new Camera(0, 0, getScreenWidth(), getScreenHeight(), 20)); //TODO hardcoded

        // WINDOW
        //TODO setup new window

        setupWindow();

        // INPUT LISTENERS
        // TODO can move these methods into a seperate function
        // TODO add input to the new window system

        // TODO MOVE TO A FUNCTION??
        // TICK RATE
        if (tickRateCap > 0) {
            this.tickRateCap = tickRateCap;
        }
        else {
            throw new IllegalArgumentException();
        }
        setTickRate(0);

        //GAME WORLD
        setGameWorld(new GameWorld(16, getRenderer().getScale(),4, 50, 50,  rand)); //TODO hardcoded

        // GAME STATE
        setGameState(GameState.TITLE);


    }

    public void setFullScreen() {
        //TODO Switch to how new window handles fullscreen


    }
    public void setupGamePanel() {
        //TODO move stuff from constructor into here

    }

    public void setupWindow() {

        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the window will be resizable

        // Create the window
        window = glfwCreateWindow(getScreenWidth(), getScreenHeight(), "Dungeon delver game", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    @Override
    public void run() {

        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        try {
            gameLoop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(window);
            glfwDestroyWindow(window);

        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }
    }

    public void gameLoopInit() {
        // Set the clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    }
    public void gameLoop() {
        final double SECOND = 1.0;
        double tickInterval = SECOND / getTickRateCap();
        double renderInterval = SECOND / getRenderer().getFpsCap();
        double tickDelta = 0;
        double renderDelta = 0;
        double lastTime = glfwGetTime();
        double currentTime;
        double timePassed;
        double timer = 0;
        int tickCount = 0;

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.

        GL.createCapabilities();

        gameLoopInit();

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while ( !glfwWindowShouldClose(window) ) {
            currentTime = glfwGetTime();
            timePassed = currentTime - lastTime;
            lastTime = currentTime;

            tickDelta += timePassed / tickInterval;
            renderDelta += timePassed / renderInterval;
            timer += timePassed;

            while (tickDelta >= 1) {
                update();
                tickCount++;
                tickDelta--;
                if (tickCount == getTickRateCap()) {
                    // this has not been tested. Sometimes
                    // the total numer of tick can
                    // overshoot the intended tick cap
                    // if this is the case, just reset
                    tickDelta = 0;
                }
            }

            if (renderDelta >= 1 || !getRenderer().isFpsCapOn()) {
                render();
                renderDelta--;
            }

            if (timer >= SECOND) {
                setTickRate(tickCount);
                getRenderer().setFps(getRenderer().getFrameCount());
                System.out.println(getRenderer().getFps());
                System.out.println(getTickRate() + "\n");
                getRenderer().setFrameCount(0);
                tickCount = 0;
                timer = 0;

            }


            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();


        }

    }




    public void update() {
        getActionManager().update(getRenderer(), getCamera());
        getGameWorld().update(getRenderer().getScale(), getRenderer().isScaleChanged());
        getRenderer().update();
    }

    public void render() {

        // Helps with performance on linux before rendering
        if (OSValidator.isUnix()) {
            //Toolkit.getDefaultToolkit().sync();
            //TODO Cause errors with low fps, maybe this is not neccessary??
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

        getRenderer().render(getGameWorld(), getCamera(), getTickRate());

        glfwSwapBuffers(window); // swap the color buffers
    }


    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public void setSoundManager(SoundManager soundManager) {
        this.soundManager = soundManager;
    }

    public ActionManager getActionManager() {
        return actionManager;
    }

    public void setActionManager(ActionManager actionManager) {
        this.actionManager = actionManager;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public int getTickRateCap() {
        return tickRateCap;
    }


    public int getTickRate() {
        return tickRate;
    }

    public void setTickRate(int tickRate) {
        if (tickRate >= 0) {
            this.tickRate = tickRate;
        }
    } public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public Random getRand() {
        return rand;
    }

    public void setRand(Random rand) {
        this.rand = rand;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public long getWindow() {
        return window;
    }

    public void setWindow(long window) {
        this.window = window;
    }


    public static void main(String[] args) {

        //TODO These hardCoded values should be variables or read in

        GamePanel gamePanel = new GamePanel(30, 165, true);
        gamePanel.run();


    }


}
